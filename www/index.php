<h1>Hello Cloudreach!</h1>
<h4>Attempting MySQL connection from php...</h4>
<?php
$host = $_ENV['DRUPAL_DB_HOST'];
$user = $_ENV['DRUPAL_DB_USER'];
$pass = $_ENV['DRUPAL_DB_PASSWORD'];
$conn = new mysqli($host, $user, $pass);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected to MySQL successfully!";
?>
