# Docker Compose LAMP stack

###### Based on the Docker images, built to be fast, small and extendable LAMP stack.

## Requirements

### Linux

Install [Docker](https://docs.docker.com/engine/installation) and [Docker Compose](https://docs.docker.com/compose/install).

## Introduction

### Linux

Run inside the folder with docker-compose.yml:
```bash
docker-compose up -d
```

To run the **PHP** script (connect to mysql database) with the **Apache2** use the IP: http://172.0.0.1

**PhpMyAdmin** is available under: http://172.0.0.1:8080
